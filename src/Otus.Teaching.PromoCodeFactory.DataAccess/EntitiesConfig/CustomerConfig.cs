using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntitiesConfig
{
    public class CustomerConfig : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder
                .HasKey(c => c.Id);

            builder
                .Property(c => c.FirstName)
                .HasMaxLength(64);

            builder
                .Property(c => c.LastName)
                .HasMaxLength(64);

            builder
                .Property(c => c.Email)
                .HasMaxLength(254);

            builder
                .HasIndex(c => c.Email)
                .IsUnique();

            builder
                .HasMany(c => c.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity<CustomerPreference>(
                    cpr => cpr
                        .HasOne(cp => cp.Preference)
                        .WithMany(p => p.CustomerPreferences)
                        .HasForeignKey(cp => cp.PreferenceId),
                    cpl => cpl
                        .HasOne(cp => cp.Customer)
                        .WithMany(c => c.CustomerPreferences)
                        .HasForeignKey(cp => cp.CustomerId),
                    cpj =>
                    {
                        cpj.HasKey(cp => new
                        {
                            cp.CustomerId,
                            cp.PreferenceId
                        });
                    });
        }
    }
}