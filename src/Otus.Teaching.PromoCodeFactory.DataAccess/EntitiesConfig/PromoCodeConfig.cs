using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntitiesConfig
{
    public class PromoCodeConfig : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder
                .HasKey(pc => pc.Id);

            builder
                .Property(pc => pc.Code)
                .HasMaxLength(32);

            builder
                .Property(pc => pc.ServiceInfo)
                .HasMaxLength(128);

            builder
                .Property(pc => pc.PartnerName)
                .HasMaxLength(128);

            builder
                .HasOne(pc => pc.Preference)
                .WithMany(p => p.PromoCodes)
                .HasForeignKey(pc => pc.PreferenceId)
                .HasPrincipalKey(p => p.Id)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(pc => pc.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(pc => pc.CustomerId)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}