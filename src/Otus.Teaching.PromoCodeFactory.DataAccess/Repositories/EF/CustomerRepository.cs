using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EF
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(Context context) : base(context)
        {
        }

        public override async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await Task.FromResult(Context.Customers
                .Include(c => c.Preferences)
                .AsEnumerable());
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            return await Task.FromResult(Context.Customers
                .Include(c => c.Preferences)
                .Include(c => c.PromoCodes)
                .FirstOrDefault(c => c.Id == id));
        }
    }
}