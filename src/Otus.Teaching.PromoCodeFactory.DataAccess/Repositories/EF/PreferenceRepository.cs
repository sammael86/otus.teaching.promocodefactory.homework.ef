using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.EF
{
    public class PreferenceRepository : EfRepository<Preference>, IPreferenceRepository
    {
        public PreferenceRepository(Context context) : base(context)
        {
        }

        public override async Task<Preference> GetByIdAsync(Guid id)
        {
            return await Task.FromResult(Context.Preferences
                .Include(p => p.Customers)
                .FirstOrDefault(p => p.Id == id));
        }
    }
}