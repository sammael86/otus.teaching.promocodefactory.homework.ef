using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class DbInitializer
    {
        public static void Initialize(Context context)
        {
            context.Database.Migrate();

            if (context.Roles.Any())
                return;

            context.Roles.AddRange(FakeDataFactory.Roles);
            context.Preferences.AddRange(FakeDataFactory.Preferences);
            context.Employees.AddRange(FakeDataFactory.Employees);
            context.Customers.AddRange(FakeDataFactory.Customers);

            context.SaveChanges();
        }
    }
}