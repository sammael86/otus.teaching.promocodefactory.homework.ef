using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class ContextFactory : IDesignTimeDbContextFactory<Context>
    {
        public Context CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<Context>();

            optionsBuilder
                .UseSqlite("FileName=promocodefactory.sqlite");

            return new Context(optionsBuilder.Options);
        }
    }
}