﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    ///     Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomersController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        /// <summary>
        ///     Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(c =>
                new CustomerShortResponse
                {
                    Id = c.Id,
                    FirstName = c.FirstName,
                    LastName = c.LastName,
                    Email = c.Email
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        ///     Получить данные клиента по id, включая списки предпочтений и выданных промокодов
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.Preferences?
                    .Select(p => new PreferenceResponse
                    {
                        Id = p.Id,
                        Name = p.Name
                    }).ToList(),
                PromoCodes = customer.PromoCodes?
                    .Select(pc => new PromoCodeShortResponse
                    {
                        Id = pc.Id,
                        Code = pc.Code,
                        ServiceInfo = pc.ServiceInfo,
                        BeginDate = pc.BeginDate.ToShortDateString(),
                        EndDate = pc.EndDate.ToShortDateString(),
                        PartnerName = pc.PartnerName
                    }).ToList()
            };

            return customerModel;
        }

        /// <summary>
        ///     Создание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request.Email is null || request.FirstName is null)
                return BadRequest();
            
            var customerId = Guid.NewGuid();
            var customer = new Customer
            {
                Id = customerId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = request.PreferenceIds?
                    .Select(preferenceId => new CustomerPreference
                    {
                        CustomerId = customerId,
                        PreferenceId = preferenceId
                    }).ToList()
            };

            await _customerRepository.AddAsync(customer);

            return Ok(new {customer.Id});
        }

        /// <summary>
        ///     Обновление данных о клиенте вместе с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request.Email is null || request.FirstName is null)
                return BadRequest();
            
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.CustomerPreferences = request.PreferenceIds?
                .Select(preferenceId => new CustomerPreference
                {
                    CustomerId = customer.Id,
                    PreferenceId = preferenceId
                }).ToList();

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        ///     Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}