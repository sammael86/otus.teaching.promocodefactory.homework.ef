﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    ///     Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IPreferenceRepository _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository,
            IPreferenceRepository preferenceRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        ///     Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var promoCodesList = promoCodes.Select(pc =>
                new PromoCodeShortResponse
                {
                    Id = pc.Id,
                    Code = pc.Code,
                    ServiceInfo = pc.ServiceInfo,
                    BeginDate = pc.BeginDate.ToShortDateString(),
                    EndDate = pc.EndDate.ToShortDateString(),
                    PartnerName = pc.PartnerName
                }).ToList();

            return Ok(promoCodesList);
        }

        /// <summary>
        ///     Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            if (request.Preference is null || request.PromoCode is null || request.PartnerName is null)
                return BadRequest();

            var preferenceId = Guid.Parse(request.Preference);
            var preference = await _preferenceRepository.GetByIdAsync(preferenceId);

            if (preference is null)
                return NotFound("Preference not found");

            if (!preference.Customers.Any())
                return NotFound("No Customers with this preference");

            var promoCodes = preference.Customers
                .Select(c => new PromoCode
                {
                    Id = Guid.NewGuid(),
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Code = request.PromoCode,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(30),
                    PreferenceId = preferenceId,
                    CustomerId = c.Id
                });

            await _promoCodeRepository.AddRangeAsync(promoCodes);

            return Ok();
        }
    }
}